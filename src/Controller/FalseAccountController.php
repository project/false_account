<?php

namespace Drupal\false_account\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Returns responses for False account routes.
 */
final class FalseAccountController extends ControllerBase {

  /**
   * Change status.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse|\Symfony\Component\HttpFoundation\Response
   *   Response.
   */
  public function falseAccountChangeStatus(Request $request) {
    $new_status = $request->attributes->get('new_status');
    $cid = $request->attributes->get('cid');

    $incorrect_response = new Response();
    $incorrect_response->setContent('Incorrect request.');
    $incorrect_response->setStatusCode(400);

    if ($new_status === NULL || !$cid) {
      return $incorrect_response;
    }

    if ($new_status < 0 || $new_status > 2) {
      return $incorrect_response;
    }

    $entities = $this->entityTypeManager()->getStorage('false_account')->loadByProperties([
      'cid' => $cid,
    ]);

    if ($entities) {
      $uids = [];

      foreach ($entities as $entity) {
        /** @var \Drupal\false_account\FalseAccountInterface $entity */
        $entity->setNewStatus($new_status);
        $entity->save();
        $uids[] = $entity->get('uid')->target_id;
      }

      $url = NULL;
      $users = $this->entityTypeManager()->getStorage('user')->loadMultiple($uids);

      switch ($new_status) {
        case 0:
          $url = Url::fromRoute('view.false_account_default.page_1', [], ['absolute' => TRUE]);
          /** @var \Drupal\user\UserInterface $user */
          foreach ($users as $user) {
            $user->activate();
            $user->save();
          }
          break;

        case 1:
          $url = Url::fromRoute('view.false_account_whitelisted.page_1', [], ['absolute' => TRUE]);
          /** @var \Drupal\user\UserInterface $user */
          foreach ($users as $user) {
            $user->activate();
            $user->save();
          }
          break;

        case 2:
          $url = Url::fromRoute('view.false_account_blocked.page_1', [], ['absolute' => TRUE]);
          /** @var \Drupal\user\UserInterface $user */
          foreach ($users as $user) {
            $user->block();
            $user->save();
          }
          break;
      }

      return new RedirectResponse($url->toString());
    }

    return $incorrect_response;
  }

}
