<?php

namespace Drupal\false_account;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining a false account entity type.
 */
interface FalseAccountInterface extends ContentEntityInterface {

  /**
   * Set new status.
   */
  public function setNewStatus(int $new_status);

}
