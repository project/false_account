<?php

namespace Drupal\false_account\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\false_account\FalseAccountInterface;

/**
 * Defines the false account entity class.
 *
 * @ContentEntityType(
 *   id = "false_account",
 *   label = @Translation("False account"),
 *   label_collection = @Translation("False accounts"),
 *   label_singular = @Translation("false account"),
 *   label_plural = @Translation("false accounts"),
 *   label_count = @PluralTranslation(
 *     singular = "@count false accounts",
 *     plural = "@count false accounts",
 *   ),
 *   handlers = {
 *     "views_data" = "Drupal\views\EntityViewsData",
 *   },
 *   base_table = "false_account",
 *   admin_permission = "administer false account",
 *   entity_keys = {
 *     "id" = "id",
 *   },
 * )
 */
final class FalseAccount extends ContentEntityBase implements FalseAccountInterface {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['cid'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Cookie ID'))
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('User'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default');

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['status'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Status'))
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function setNewStatus(int $new_status) {
    $this->set('status', $new_status);
  }

}
