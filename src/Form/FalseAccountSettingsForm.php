<?php

namespace Drupal\false_account\Form;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure False account settings.
 */
final class FalseAccountSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'false_account_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['false_account.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['redirect'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Redirect blocked users'),
      '#description' => $this->t('Indicate the Drupal internal URL to redirect to or insert an external URL.'),
      '#default_value' => $this->config('false_account.settings')->get('redirect'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    $url = $form_state->getValue('redirect');
    if (($url !== '<front>') && !UrlHelper::isValid($url)) {
      $form_state->setErrorByName('redirect', $this->t('The url is not valid.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('false_account.settings')
      ->set('redirect', $form_state->getValue('redirect'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
