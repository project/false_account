# CONTENTS OF THIS FILE

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


## INTRODUCTION

Detect false accounts.

 * For a full description of the module, visit the project page:
   <https://drupal.org/project/false_account>

 * To submit bug reports and feature suggestions, or to track changes:
   <https://drupal.org/project/issues/false_account>


## REQUIREMENTS

Drupal core nodes and Drupal core comments.


## INSTALLATION

Install as you would normally install a contributed Drupal module. Visit:
<https://www.drupal.org/node/1897420> for further information.


## CONFIGURATION

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Configuration > People 
       > False Account Detector > Settings
    3. Save configuration.


## MAINTAINERS

 * Andrei Ivnitskii - <https://www.drupal.org/u/ivnish>
