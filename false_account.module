<?php

/**
 * @file
 * Primary module hooks for False account module.
 */

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\false_account\Entity\FalseAccount;
use Drupal\false_account\FalseAccountInterface;
use Drupal\user\UserInterface;
use Drupal\views\Plugin\views\query\QueryPluginBase;
use Drupal\views\ViewExecutable;
use Drupal\views\Views;

/**
 * Implements hook_views_query_alter().
 */
function false_account_views_query_alter(ViewExecutable $view, QueryPluginBase $query) {
  if (($view->id() === 'false_account_search') && isset($view->exposed_data['uid'][0]['target_id'])) {
    $uid = $view->exposed_data['uid'][0]['target_id'];
    $entities = \Drupal::entityTypeManager()->getStorage('false_account')->loadByProperties([
      'uid' => $uid,
    ]);
    if ($entities) {
      foreach ($entities as $entity) {
        if ($entity) {
          $cid = $entity->get('cid')->value;
          if ($cid) {
            $query->setWhereGroup('OR', 1);
            $query->addWhere(1, 'false_account.cid', $cid, '=');
          }
        }
      }
    }
    $where_clause = 'cid IN (SELECT cid FROM {false_account} GROUP BY cid HAVING COUNT(*) > 1)';
    $query->setWhereGroup('AND', 2);
    $query->addWhereExpression(2, $where_clause);
  }

  if (($view->id() === 'false_account_block') && isset($view->argument['uid'])) {
    $uid = $view->argument['uid']->getValue();
    $entities = \Drupal::entityTypeManager()->getStorage('false_account')->loadByProperties([
      'uid' => $uid,
    ]);
    if ($entities) {
      foreach ($entities as $entity) {
        if ($entity) {
          $cid = $entity->get('cid')->value;
          if ($cid) {
            $query->setWhereGroup('OR', 0);
            $query->addWhere(0, 'false_account.cid', $cid, '=');
          }
        }
      }
    }
    $where_clause = 'cid IN (SELECT cid FROM {false_account} GROUP BY cid HAVING COUNT(*) > 1)';
    $query->setWhereGroup('AND', 1);
    $query->addWhereExpression(1, $where_clause);
  }

  // Do not show rows where CID is unique.
  if (
    $view->id() === 'false_account_default' ||
    $view->id() === 'false_account_blocked' ||
    $view->id() === 'false_account_whitelisted'
  ) {
    $where_clause = 'cid IN (SELECT cid FROM {false_account} GROUP BY cid HAVING COUNT(*) > 1)';
    $query->setWhereGroup('OR', 0);
    $query->addWhereExpression(0, $where_clause);
  }
}

/**
 * Implements hook_entity_extra_field_info().
 */
function false_account_entity_extra_field_info() {
  $extra = [];

  $extra['user']['user']['display']['false_account'] = [
    'label' => t('False accounts'),
    'weight' => 100,
    'visible' => FALSE,
  ];

  return $extra;
}

/**
 * Implements hook_ENTITY_TYPE_view().
 */
function false_account_user_view(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, $view_mode) {
  if ($display->getComponent('false_account') && \Drupal::currentUser()->hasPermission('administer false account')) {
    $view = Views::getView('false_account_block');
    if ($view) {
      $block = $view->buildRenderable('block_1', [$entity->id()]);
      if ($block) {
        $build['false_account_block'] = [
          '#type' => 'container',
          '#attributes' => ['class' => ['views-block-wrapper']],
          'content' => $block,
          '#prefix' => '<h4>' . t('False Account') . '</h4>',
        ];
      }
    }
  }
}

/**
 * Implements hook_user_login().
 */
function false_account_user_login(UserInterface $account) {
  if ($account->id() === '1' || $account->hasPermission('administer false account')) {
    return;
  }

  if (!\Drupal::request()->cookies->get('fad')) {
    $unique_id = md5('gsmi789' . uniqid(mt_rand(), TRUE));
    setcookie('fad', $unique_id . ',' . $account->id(), time() + 31536000, '/');
    FalseAccount::create([
      'cid' => $unique_id,
      'uid' => $account->id(),
      'created' => time(),
      'status' => 0,
    ])->save();
  }
  else {
    $message = t('Cannot login. Please contact with site administration.');
    $cookie = \Drupal::request()->cookies->get('fad');
    $cdata = explode(',', $cookie);
    $cid = $cdata[0];

    $entities = \Drupal::entityTypeManager()->getStorage('false_account')->loadByProperties([
      'cid' => $cid,
    ]);
    $status = 0;
    $entity = reset($entities);
    if ($entity) {
      $status = $entity->get('status')->value;
    }

    // If user not added to cookie.
    if (!in_array($account->id(), $cdata, TRUE)) {
      $new_cdata = $cookie . ',' . $account->id();
      setcookie('fad', $new_cdata, time() + 31536000, '/');

      FalseAccount::create([
        'cid' => $cid,
        'uid' => $account->id(),
        'created' => time(),
        'status' => $status,
      ])->save();
    }

    if ($status === '2' || count($entities) >= 3) {
      $account->block();
      $account->save();
      \Drupal::messenger()->addMessage($message, MessengerInterface::TYPE_WARNING, TRUE);
    }
  }
}

/**
 * Implements hook_help().
 */
function false_account_help($route_name, RouteMatchInterface $route_match) {
  if ($route_name === 'help.page.false_account') {
    $text = file_get_contents(__DIR__ . '/README.md');
    return '<pre>' . Html::escape($text) . '</pre>';
  }
  return '';
}

/**
 * Implements hook_cron().
 */
function false_account_cron() {
  $entities = \Drupal::entityQuery('false_account')
    ->accessCheck(FALSE)
    ->condition('created', time() - 31536000, '<')
    ->execute();

  if ($entities) {
    foreach ($entities as $entity_id) {
      $entity = \Drupal::entityTypeManager()->getStorage('false_account')->load($entity_id);
      if ($entity instanceof FalseAccountInterface) {
        $entity->delete();
      }
    }
  }
}
