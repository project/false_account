<?php

namespace Drupal\Tests\false_account\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests for the false_account module.
 *
 * @group false_account
 */
class FalseAccountUserBlockTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = [
    'false_account',
    'views_aggregator',
    'field_ui',
  ];

  /**
   * The User used for the test.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  private $adminUser;

  /**
   * The User used for the test.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  private $user;

  /**
   * SetUp the test class.
   */
  public function setUp(): void {
    parent::setUp();
    $this->user = $this->DrupalCreateUser();
    $this->adminUser = $this->DrupalCreateUser([
      'administer false account',
      'administer account settings',
      'administer user display',
      'access administration pages',
    ]);
  }

  /**
   * Tests that the user block can be reached.
   */
  public function testUserBlock() {
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('/admin/config/people/accounts/display');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('False accounts');
    $this->getSession()->getPage()->selectFieldOption('fields[false_account][region]', 'content');
    $this->submitForm([], 'Save');
    $this->assertTrue($this->assertSession()->optionExists('fields[false_account][region]', 'content')->isSelected());

    $this->drupalLogin($this->user);
    $this->assertSession()->pageTextNotContains('False Account');

    $this->drupalLogin($this->adminUser);
    $this->assertSession()->pageTextContains('No false accounts detected');
  }

}
