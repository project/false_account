<?php

namespace Drupal\Tests\false_account\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests for the false_account module.
 *
 * @group false_account
 */
class FalseAccountPagesTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = [
    'false_account',
    'views_aggregator',
  ];

  /**
   * The User used for the test.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  private $adminUser;

  /**
   * The User used for the test.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  private $user1;

  /**
   * The User used for the test.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  private $user2;

  /**
   * SetUp the test class.
   */
  public function setUp(): void {
    parent::setUp();
    $this->user1 = $this->DrupalCreateUser();
    $this->user2 = $this->DrupalCreateUser();
    $this->adminUser = $this->DrupalCreateUser([
      'administer false account',
    ]);
  }

  /**
   * Tests that the default page can be reached.
   */
  public function testDefaultPage() {
    $this->drupalLogin($this->user1);
    $this->drupalLogin($this->user2);
    $this->drupalGet('admin/user/false_account');
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/user/false_account');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementsCount('css', 'table tbody tr', 1);
  }

  /**
   * Tests that the whitelisted page can be reached.
   */
  public function testWhitelistedPage() {
    $this->drupalLogin($this->user1);
    $this->drupalLogin($this->user2);
    $this->drupalGet('admin/user/false_account/whitelisted');
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/user/false_account');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementsCount('css', 'table tbody tr', 1);
    $this->getSession()->getPage()->clickLink('whitelist');
    $this->drupalGet('admin/user/false_account/whitelisted');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementsCount('css', 'table tbody tr', 1);
  }

  /**
   * Tests that the blocked page can be reached.
   */
  public function testBlockedPage() {
    $user_storage = $this->container->get('entity_type.manager')->getStorage('user');
    $this->drupalLogin($this->user1);
    $this->drupalLogin($this->user2);
    $this->drupalGet('admin/user/false_account/blocked');
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/user/false_account');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementsCount('css', 'table tbody tr', 1);
    $this->getSession()->getPage()->clickLink('block');
    $this->drupalGet('admin/user/false_account/blocked');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementsCount('css', 'table tbody tr', 1);
    // Move cookie to blocked list also block all user accounts of this cookie.
    $user_storage->resetCache([$this->user1->id()]);
    $account = $user_storage->load($this->user1->id());
    $this->assertTrue($account->isBlocked());
    $user_storage->resetCache([$this->user2->id()]);
    $account = $user_storage->load($this->user2->id());
    $this->assertTrue($account->isBlocked());
    $this->getSession()->getPage()->clickLink('default');
    $this->drupalGet('admin/user/false_account');
    $this->assertSession()->elementsCount('css', 'table tbody tr', 1);
    // Move cookie to default list also unblock all user accounts of this
    // cookie.
    $user_storage->resetCache([$this->user1->id()]);
    $account = $user_storage->load($this->user1->id());
    $this->assertTrue($account->isActive());
    $user_storage->resetCache([$this->user2->id()]);
    $account = $user_storage->load($this->user2->id());
    $this->assertTrue($account->isActive());
  }

  /**
   * Tests that the search page can be reached and saved.
   */
  public function testSearchPage() {
    $this->drupalLogin($this->user1);
    $this->drupalLogin($this->user2);
    $this->drupalGet('admin/user/false_account/search');
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/user/false_account/search');
    $this->assertSession()->statusCodeEquals(200);
    $this->getSession()->getPage()->fillField('uid', $this->user1->getAccountName());
    $this->getSession()->getPage()->pressButton('Apply');
    $this->assertSession()->elementsCount('css', 'table tbody tr', 1);
  }

}
