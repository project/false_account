<?php

namespace Drupal\Tests\false_account\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests for the false_account module.
 *
 * @group false_account
 */
class FalseAccountUserLoginTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = [
    'false_account',
    'views_aggregator',
  ];

  /**
   * The User used for the test.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  private $user1;

  /**
   * The User used for the test.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  private $user2;

  /**
   * The User used for the test.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  private $user3;

  /**
   * The User used for the test.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  private $user4;

  /**
   * SetUp the test class.
   */
  public function setUp(): void {
    parent::setUp();
    $this->user1 = $this->DrupalCreateUser();
    $this->user2 = $this->DrupalCreateUser();
  }

  /**
   * Tests block account log in if cookie blocked.
   */
  public function testLoginIfCookieBlocked() {
    // Test multiple logins.
    $this->drupalLogin($this->user1);
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalLogin($this->user2);
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalLogin($this->user1);
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalLogin($this->user2);
    $this->assertSession()->statusCodeEquals(200);

    // Check that entities with cookie cid exists.
    $entities = $this->container->get('entity_type.manager')->getStorage('false_account')->loadMultiple();
    $this->assertCount(2, $entities);
    /** @var \Drupal\false_account\FalseAccountInterface $entity */
    foreach ($entities as $entity) {
      // Check cookies.
      $this->assertSession()->cookieEquals('fad', $entity->get('cid')->value . ',2,3');
      // Programmatically add these cookies to block list.
      $entity->setNewStatus(2);
      $entity->save();
    }

    $this->user3 = $this->DrupalCreateUser();
    $this->drupalLogin($this->user3);
    // User cannot log in because cookie is in block list.
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Tests block account log in if user has more than 3 accounts.
   */
  public function testLoginIfThreeAccounts() {
    // Test multiple logins.
    $this->drupalLogin($this->user1);
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalLogin($this->user2);
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalLogin($this->user1);
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalLogin($this->user2);
    $this->assertSession()->statusCodeEquals(200);
    $this->user3 = $this->DrupalCreateUser();
    $this->drupalLogin($this->user3);
    // Now this cookie has 3 accounts.
    $this->user4 = $this->DrupalCreateUser();
    // Block login.
    $this->drupalLogin($this->user4);
    $this->assertSession()->statusCodeEquals(403);
  }

}
