<?php

namespace Drupal\Tests\false_account\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests for the false_account module.
 *
 * @group false_account
 */
class FalseAccountSettingsFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = [
    'false_account',
    'node',
    'views_aggregator',
  ];

  /**
   * The User used for the test.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  private $adminUser;

  /**
   * The User used for the test.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  private $user;

  /**
   * Settings form url.
   */
  protected Url $settingsRoute;

  /**
   * SetUp the test class.
   */
  public function setUp(): void {
    parent::setUp();
    $this->settingsRoute = Url::fromRoute('false_account.settings');
    $this->user = $this->DrupalCreateUser();
    $this->adminUser = $this->DrupalCreateUser([
      'administer false account',
    ]);
    $this->createContentType(['type' => 'page']);
    $this->createNode([
      'title' => 'Site rules',
      'type' => 'page',
    ]);
  }

  /**
   * Tests that the settings page can be reached and saved.
   */
  public function testSettingsPage() {
    $this->drupalLogin($this->user);
    $this->drupalGet($this->settingsRoute);
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalLogin($this->adminUser);
    $this->drupalGet($this->settingsRoute);
    $this->assertSession()->statusCodeEquals(200);

    $edit = [
      'redirect' => '/node/1',
    ];
    $expected_values = [
      'redirect' => '/node/1',
    ];

    $this->submitForm($edit, 'Save configuration');
    $this->assertSession()->pageTextContains('The configuration options have been saved.');

    foreach ($expected_values as $field => $expected_value) {
      $actual_value = $this->config('false_account.settings')->get($field);
      $this->assertEquals($expected_value, $actual_value);
    }
  }

}
